require('es6-promise').polyfill();

import React from 'react';
import fetch from 'isomorphic-fetch';

import './styles/index.css';

import Tile from './components/Tile';

class WebApp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      creditcards: []
    };
  }

  componentWillMount() {
    fetch('/~/services/creditcards')
      .then(function(response) {
        if (response.status >= 400) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then((data) => {
        this.setState(() => ({
          creditcards: data.creditcards,
        }));
      });
  }

  applyBtn = (event) => {
    event.preventDefault();
    console.log(event.target);
  }


  termBtn = (event) => {
    event.preventDefault();
    console.log(event.target);
  }

  viewBtn = (event) => {
    event.preventDefault();
    console.log(event.target);
  }

  learnMoreBtn = (event) => {
    event.preventDefault();
    console.log(event.target);
  }

  render() {
    // return  (
    //   <main>
    //     <div className="container">
    //       <div className="row">
    //         {this.state.creditcards.map((data) => {
    //           switch (data.type) {
    //           case 'a':
    //             return (
    //               <ComponentA {...data} />
    //             );
    //           case 'b':
    //             return (
    //               <ComponentB {...data}
    //                 applyBtn={(event) => {
    //                   event.preventDefault();
    //                   console.log(event.target);
    //                 }}
    //                 termBtn={(event) => {
    //                   event.preventDefault();
    //                   console.log(event.target);
    //                 }}
    //                 learnMoreBtn={(event) => {
    //                   event.preventDefault();
    //                   console.log(event.target);
    //                 }} />
    //             );
    //           case 'c':
    //             return (
    //               <ComponentC {...data} />
    //             );
    //           case 'd':
    //             return (
    //               <ComponentD {...data}
    //                 applyBtn={(event) => {
    //                   event.preventDefault();
    //                   console.log(event.target);
    //                 }}
    //                 viewBtn={(event) => {
    //                   event.preventDefault();
    //                   console.log(event.target);
    //                 }} />
    //             );
    //           case 'e':
    //             return (
    //               <ComponentE {...data} />
    //             );
    //           case 'f':
    //             return (
    //               <ComponentF {...data}
    //                 applyBtn={(event) => {
    //                   event.preventDefault();
    //                   console.log(event.target);
    //                 }}
    //                 learnMoreBtn={(event) => {
    //                   event.preventDefault();
    //                   console.log(event.target);
    //                 }} />
    //             );
    //           case 'g':
    //             return (
    //               <ComponentG {...data}
    //                 applyBtn={(event) => {
    //                   event.preventDefault();
    //                   console.log(event.target);
    //                 }}
    //                 learnMoreBtn={(event) => {
    //                   event.preventDefault();
    //                   console.log(event.target);
    //                 }} />
    //             );
    //           case 'h':
    //             return (
    //               <ComponentH {...data}
    //                 applyBtn={(event) => {
    //                   event.preventDefault();
    //                   console.log(event.target);
    //                 }} />
    //             );
    //           default:
    //             return (
    //               <ComponentA {...data} />
    //             );
    //           }
    //         })}
    //       </div>
    //     </div>
    //   </main>
    // );

    return (
      <main>
        <div className="container">
          <div className="row">
            {
              this.state.creditcards.map((data, index) => <Tile {...data}
                key={index}                   
                applyBtn={this.applyBtn}
                termBtn={this.termBtn}
                viewBtn={this.viewBtn}
                learnMoreBtn={this.learnMoreBtn} 
              />)
            }
          </div>
        </div>
      </main>
    );
  }
}

export default WebApp;