import React from 'react';
import PropTypes from 'prop-types';

import { CardTitle } from './CardTitle';
import { CardDescription } from './CardDescription';


class CardBody extends React.PureComponent {
  render() {
    return (
      <div style={this.props.style} className={`card-block ${this.props.className}`}>
        {
          this.props.title ?
            <CardTitle title={this.props.title} className={this.props.titleStyleNames} style={this.props.titleStyle}  />
            :
            null
        }
        {
          this.props.button ?
            <div style={this.props.buttonContainerStyle} className={`meta ${this.props.buttonContainerStyleNames}`}>
              {this.props.button}
            </div>
            :
            null
        }
        {
          this.props.description ?
            <CardDescription className={this.props.descriptionStyleNames} style={this.props.descriptionStyle} description={this.props.description} />
            :
            null
        }
        { this.props.children }
      </div>
    );
  }
}


CardBody.propTypes = {
  title: PropTypes.string,
  button: PropTypes.element,
  description: PropTypes.string,
  children:  PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  buttonContainerStyleNames: PropTypes.string,
  titleStyleNames: PropTypes.string,
  titleStyle: PropTypes.object,
  buttonContainerStyle: PropTypes.object,
  descriptionStyle: PropTypes.object,
  descriptionStyleNames: PropTypes.string
};

CardBody.defaultProps = {
  buttonContainerStyle: {},
  className: '',
  buttonContainerStyleNames: '',
  style: {}
};

export { CardBody };