import React from 'react';
import PropTypes from 'prop-types';

class CardTitle extends React.PureComponent {
  render() {
    return (
      <h4 style={this.props.style} className={`card-title ${this.props.className}`} >{this.props.title}</h4>
    );
  }
}

CardTitle.propTypes = {
  title: PropTypes.string.isRequired,
  style: PropTypes.object,
  className: PropTypes.string
};

CardTitle.defaultProps = {
  className: '',
  style: {}
};

export { CardTitle };