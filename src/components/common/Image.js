import React from 'react';
import PropTypes from 'prop-types';

class Image extends React.PureComponent {
  render() {
    return (
      <img style={this.props.style} className={`card-img-top ${this.props.className}`} src={this.props.src} />      
    );
  }
}

Image.propTypes = {
  src: PropTypes.string.isRequired,
  style: PropTypes.object,
  className: PropTypes.string
};

Image.defaultProps = {
  style: {},
  className: ''
};

export { Image };