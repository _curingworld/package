import React from 'react';
import PropTypes from 'prop-types';

class CardDescription extends React.PureComponent {
  render() {
    return (
      <div style={this.props.style} className={`card-text ${this.props.className}`}>{this.props.description}</div>
    );
  }
}


CardDescription.propTypes = {
  description: PropTypes.string.isRequired,
  style: PropTypes.object,
  className: PropTypes.string.isRequired
};


CardDescription.defaultProps = {
  style: {},
  className: ''
};

export { CardDescription };