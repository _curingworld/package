import React from 'react';
import PropTypes from 'prop-types';


class CardFooter extends React.PureComponent {
  render() {
    return (
      <div style={this.props.style} className={`card-footer ${this.props.className}`}>
        {this.props.children}
      </div>
    );
  }
}


CardFooter.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  className: PropTypes.string,
  style: PropTypes.object
};

CardFooter.defaultProps = {
  style: {},
  className: ''
};



export { CardFooter };