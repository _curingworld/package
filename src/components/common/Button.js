import React from 'react';
import PropTypes from 'prop-types';

class Button extends React.PureComponent {
  render() {
    return (
      <button style={this.props.style} className={`btn ${this.props.className}`} onClick={this.props.onClick}>{this.props.title}</button>
    );
  }
}


Button.propTypes = {
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  style: PropTypes.object,
  className: PropTypes.string
};

Button.defaultProps = {
  style: {},
  className: '',
};

export { Button };