import React from 'react';
import PropTypes from 'prop-types';

import { Image } from './Image';

class Card extends React.Component {
  render() {
    return (
      <div style={this.props.style} className={`col-sm-6 col-md-4 col-lg-3 mt-4 ${this.props.className}`}>
        <div style={this.props.wrapperStyle} className={`card ${this.props.wrapperStyleNames}`}>
          <Image src={this.props.image} className={this.props.imageStyleNames} style={this.props.imageStyle} />
          {this.props.children}
        </div>
      </div>
    );
  }
}

Card.propTypes = {
  image: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  wrapperStyleNames: PropTypes.string,
  imageStyleNames: PropTypes.string,
  wrapperStyle: PropTypes.object,
  imageStyle: PropTypes.object,
  style: PropTypes.object,
  className: PropTypes.string
};

Card.defaultProps = {
  wrapperStyleNames: '',
  wrapperStyle: {},
  style: {},
  className: ''
};

export { Card };