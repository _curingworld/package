
import React from 'react';


import PropTypes from 'prop-types';

import { Card, CardBody, CardFooter, Button } from './common';

class Component extends React.Component {
  render() {
    const {
      creditCard1, cardTitle, cardDescription, applyBtn
    } = this.props;

    // return  (
    //   <div className="col-sm-6 col-md-4 col-lg-3 mt-4">
    //       <div className="card card-inverse card-info">
    //           <img className="card-img-top" src={creditCard1} />
    //           <div className="card-block">
    //               <h4 className="card-title">{cardTitle}</h4>
    //               <div className="card-text">{cardDescription}</div>
    //           </div>
    //           <div className="card-footer">
    //               <button className="btn btn-info btn-sm" onClick={applyBtn}>Apply</button>
    //           </div>
    //       </div>
    //   </div>
    // );

    return (
      <Card image={creditCard1}>
        <CardBody 
          title={cardTitle}
          description={cardDescription}
        />
        <CardFooter>
          <Button className="btn-info btn-sm" onClick={applyBtn} title="Apply" />
        </CardFooter>
      </Card>
    );
  }
}

Component.propTypes = {
  creditCard1: PropTypes.string.isRequired, 
  cardTitle: PropTypes.string.isRequired, 
  cardDescription: PropTypes.string.isRequired, 
  footerDisclaimer: PropTypes.string.isRequired,
  applyBtn: PropTypes.func.isRequired
};

export default Component;