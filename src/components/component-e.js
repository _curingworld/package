import React from 'react';
import PropTypes from 'prop-types';

import { Card } from './common';

class Component extends React.Component {
  render() {
    const {
      creditCard1, cardDescription, footerDisclaimer
    } = this.props;

    // return  (
    //   <div className="col-sm-6 col-md-4 col-lg-3 mt-4">
    //       <div className="card card-inverse card-primary ">
    //           <img className="card-img-top" src={creditCard1} />
    //           <blockquote className="card-blockquote p-3">
    //               <p>{cardDescription}</p>
    //               <div>
    //                 <ul>
    //                   <li>benefit #1</li>
    //                   <li>benefit #2</li>
    //                   <li>benefit #3</li>
    //                   <li>benefit #4</li>
    //                 </ul>
    //               </div>
    //               <footer>
    //                 <small>{footerDisclaimer}</small>
    //               </footer>
    //           </blockquote>
    //       </div>
    //   </div>
    // );

    return (
      <Card image={creditCard1}>
        <blockquote className="card-blockquote p-3">
          <p>{cardDescription}</p>
          <div>
            <ul>
              <li>benefit #1</li>
              <li>benefit #2</li>
              <li>benefit #3</li>
              <li>benefit #4</li>
            </ul>
          </div>
          <footer>
            <small>{footerDisclaimer}</small>
          </footer>
        </blockquote>
      </Card>
    );
  }
}

Component.propTypes = {
  creditCard1: PropTypes.string.isRequired, 
  cardTitle: PropTypes.string.isRequired, 
  cardDescription: PropTypes.string.isRequired, 
  footerDisclaimer: PropTypes.string.isRequired
};

export default Component;