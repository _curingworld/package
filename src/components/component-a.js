
import React from 'react';
import PropTypes from 'prop-types';

import { Card, CardBody } from './common';

class Component extends React.PureComponent {
  render() {
    const {
      creditCard1, cardTitle
    } = this.props;

    // return  (
    //   <div className="col-sm-6 col-md-4 col-lg-3 mt-4">
    //       <div className="card">
    //           <img className="card-img-top" src={creditCard1} />
    //           <div className="card-block">
    //               <h5 className="text-bold">{cardTitle}</h5>
    //           </div>
    //       </div>
    //   </div>
    // );
    return (
      <Card image={creditCard1}>
        <CardBody>
          <h5 className="text-bold">{cardTitle}</h5>
        </CardBody>
      </Card>
    );
  }
}

Component.propTypes = {
  creditCard1: PropTypes.string.isRequired,
  cardTitle: PropTypes.string.isRequired
};

export default Component;