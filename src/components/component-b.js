
import React from 'react';
import PropTypes from 'prop-types';

import { Card, CardBody, Button, CardFooter } from './common';

class Component extends React.PureComponent {
  render() {
    const {
      creditCard1, cardTitle, cardDescription, applyBtn, learnMoreBtn, termBtn
    } = this.props;
    // return  (
    //   <div className="col-sm-6 col-md-4 col-lg-3 mt-4">
    //       <div className="card">
    //           <img className="card-img-top" src={creditCard1} />
    //           <div className="card-block">
    //               <h4 className="card-title">{cardTitle}</h4>
    //               <div className="meta">
    //                 <button className="btn btn-primary float-right btn-sm" onClick={applyBtn}>Apply</button>
    //               </div>
    //               <div className="card-text">{cardDescription}</div>
    //           </div>
    //           <div className="card-footer">
    //               <span className="float-right"><a href="" onClick={termBtn}>Terms</a></span>
    //               <span><a href="" onClick={learnMoreBtn}><i className=""></i>Learn More</a></span>
    //           </div>
    //       </div>
    //   </div>
    // );
    return (
      <Card image={creditCard1}>
        <CardBody 
          title={cardTitle} 
          description={cardDescription} 
          button={<Button className="btn-primary float-right btn-sm" onClick={applyBtn} title="Apply" />} 
        />
        <CardFooter>
          <span className="float-right"><a href="" onClick={termBtn}>Terms</a></span>
          <span><a href="" onClick={learnMoreBtn}><i className=""></i>Learn More</a></span>
        </CardFooter>
      </Card>
    );
  }
}

Component.propTypes = {
  creditCard1: PropTypes.string.isRequired, 
  cardTitle: PropTypes.string.isRequired, 
  cardDescription: PropTypes.string.isRequired, 
  applyBtn: PropTypes.func.isRequired, 
  learnMoreBtn: PropTypes.func.isRequired, 
  termBtn: PropTypes.func.isRequired
};

export default Component;