import React from 'react';
import PropTypes from 'prop-types';

import ComponentA from './component-a';
import ComponentB from './component-b';
import ComponentC from './component-c';
import ComponentD from './component-d';
import ComponentE from './component-e';
import ComponentF from './component-f';
import ComponentG from './component-g';
import ComponentH from './component-h';


const componetsMap = {
  a: ComponentA,
  b: ComponentB,
  c: ComponentC,
  d: ComponentD,
  e: ComponentE,
  f: ComponentF,
  g: ComponentG,
  h: ComponentH
};


class Tile extends React.PureComponent {
  render() {
    const Card = componetsMap[this.props.type];
    return (
      <Card {...this.props} />
    );
  }
}


Tile.propTypes = {
  type: PropTypes.string.isRequired
};

export default Tile;