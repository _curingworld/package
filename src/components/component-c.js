
import React from 'react';
import PropTypes from 'prop-types';

import { Card, CardBody } from './common';


class Component extends React.Component {
  render() {
    const {
      creditCard1, cardDescription
    } = this.props;

    // return  (
    //   <div className="col-sm-6 col-md-4 col-lg-3 mt-4">
    //       <div className="card">
    //           <img className="card-img-top" src={creditCard1} />
    //           <div className="card-block">{cardDescription}</div>
    //       </div>
    //   </div>
    // );
    return (
      <Card image={creditCard1}>
        <CardBody>{cardDescription}</CardBody>
      </Card>
    );
  }
}

Component.propTypes = {
  creditCard1: PropTypes.string.isRequired, 
  cardTitle: PropTypes.string, 
  cardDescription: PropTypes.string.isRequired
};

export default Component;