import React from 'react';
import renderer from 'react-test-renderer';

import Card from '../../../src/components/component-e';
const data = require('../data/card.json');

test('component-e renders correctly without any buttons', () => {
  const component = renderer.create(
    <Card {...data}  />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
