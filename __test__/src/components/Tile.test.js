import React from 'react';
import renderer from 'react-test-renderer';

import Tile from '../../../src/components/Tile';
const data = require('../data/card.json');

const types = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

const tests = [];
types.forEach((type, index) => {
  tests.push({ ...data, id: index, type, });
});

tests.forEach(card => {
  test(`renders correctly with type ${card.type} card`, () => {
    const component = renderer.create(
      <Tile {...card} applyBtn={() => {}} learnMoreBtn={() => {}} viewBtn={() => {}} termBtn={() => {}}   />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

