import React from 'react';
import renderer from 'react-test-renderer';

//<rootDir> was somehow not working
const { CardBody } = require('../../../../src/components/common');
const { Button } = require('../../../../src/components/common');


test('CardBody renders correctly without children', () => {
  const component = renderer.create(
    <CardBody className="foo bar" title="Hello World" description="This is awesome react" />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('CardBody renders correctly with children', () => {
  const component = renderer.create(
    <CardBody className="foo bar" title="Hello">
      This is React101
    </CardBody>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
})

test('CardBody renders correctly with button', () => {
  const component = renderer.create(
    <CardBody className="foo bar" description="Heeeeee"  button={<Button onClick={() => {}} title="Hello" />} />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
})
