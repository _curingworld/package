import React from 'react';
import renderer from 'react-test-renderer';

//<rootDir> was somehow not working

const { CardDescription } = require('../../../../src/components/common/CardDescription');

test('CardDescription renders correctly', () => {
  const component = renderer.create(
    <CardDescription style={{ fontSize: 30 }} className="foo bar" description="This is hello world" />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
