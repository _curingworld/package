import React from 'react';
import renderer from 'react-test-renderer';

//<rootDir> was somehow not working
const { CardFooter } = require('../../../../src/components/common');

test('CardFooter renders correctly', () => {
  const component = renderer.create(
    <CardFooter>
      <div>This is footer</div>
    </CardFooter>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
