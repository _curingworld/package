import React from 'react';
import renderer from 'react-test-renderer';

//<rootDir> was somehow not working
const { Image } = require('../../../../src/components/common/Image');

test('Image renders correctly', () => {
  const component = renderer.create(
    <Image style={{ width: 10 }} src="http://placehold.it/900x300" className="foo bar" />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
