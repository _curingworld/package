import React from 'react';
import renderer from 'react-test-renderer';

//<rootDir> was somehow not working

const { Card } = require('../../../../src/components/common');

test('Card renders correctly without children', () => {
  const component = renderer.create(
    <Card className="hello world" image="http://placehold.it/900x300"  />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});


test('Card renders correctly with children', () => {
  const component = renderer.create(
    <Card style={{ backgroundColor: 'pink' }} className="hello world" image="http://placehold.it/900x300" >
      <p>This is my card</p>
      <p>No, this is my card</p>
    </Card>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
