import React from 'react';
import renderer from 'react-test-renderer';

//<rootDir> was somehow not working
const { Button } = require('../../../../src/components/common');

test('Button renders correctly', () => {
  const component = renderer.create(
    <Button style={{ backgroundColor: 'blue' }} className="foo bar" onClick={() => {}} title="Hello World" />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});