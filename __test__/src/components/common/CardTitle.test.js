import React from 'react';
import renderer from 'react-test-renderer';

//<rootDir> was somehow not working
const { CardTitle } = require('../../../../src/components/common/CardTitle');

test('CardTitle renders correctly', () => {
  const component = renderer.create(
    <CardTitle style={{ width: 10 }} title="Hello World" className="foo bar" />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
