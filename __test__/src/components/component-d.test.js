import React from 'react';
import renderer from 'react-test-renderer';

import Card from '../../../src/components/component-d';
const data = require('../data/card.json');

test('component-d renders correctly with learn more and view button', () => {
  const component = renderer.create(
    <Card {...data}  learnMoreBtn={() => {}} viewBtn={() => {}}  />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
