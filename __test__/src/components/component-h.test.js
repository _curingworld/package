import React from 'react';
import renderer from 'react-test-renderer';

import Card from '../../../src/components/component-h';
const data = require('../data/card.json');

test('component-h renders correctly with apply btn', () => {
  const component = renderer.create(
    <Card {...data} applyBtn={() => {}}  />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
