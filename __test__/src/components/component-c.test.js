import React from 'react';
import renderer from 'react-test-renderer';

import Card from '../../../src/components/component-c';
const data = require('../data/card.json');

test('component-c renders correctly without any button', () => {
  const component = renderer.create(
    <Card {...data}  />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
