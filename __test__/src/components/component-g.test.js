import React from 'react';
import renderer from 'react-test-renderer';

import Card from '../../../src/components/component-g';
const data = require('../data/card.json');

test('component-g renders correctly with Apply and learn more button', () => {
  const component = renderer.create(
    <Card {...data} applyBtn={() => {}} learnMoreBtn={() => {}}   />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
