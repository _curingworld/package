import React from 'react';
import renderer from 'react-test-renderer';

import Card from '../../../src/components/component-f';
const data = require('../data/card.json');

test('component-f renders correctly with apply and learnmore buttons', () => {
  const component = renderer.create(
    <Card {...data} applyBtn={() => {}} learnMoreBtn={() => {}}   />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
