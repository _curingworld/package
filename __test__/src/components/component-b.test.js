import React from 'react';
import renderer from 'react-test-renderer';

import Card from '../../../src/components/component-b';
const data = require('../data/card.json');

test('component-b renders correctly with Apply Button, Learnmore & Term Links', () => {
  const component = renderer.create(
    <Card {...data} applyBtn={() => {}} learnMoreBtn={() => {}} termBtn={() => {}} />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
