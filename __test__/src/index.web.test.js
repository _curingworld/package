import React from 'react';
import renderer from 'react-test-renderer';

//<rootDir> was somehow not working
import WebApp from '../../src/index.web.js';


test('index.web.js renders correctly', () => {
  const component = renderer.create(
    <WebApp />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
