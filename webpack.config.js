const webpack = require('webpack');
const config = {
  entry: {
    'app': './src/index.web.js'
  },
  output: {
    path: __dirname,
    filename: 'dist/bundles/app.js'
  },
  module: {
    rules: [
      {
        test : /\.jsx?/,
        use : 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' }
        ]
      },
      {
        test: /\.svg$/,
        use: 'svg-inline-loader'
      },
      { test: /\.woff$/, use: 'url-loader?limit=65000&mimetype=application/font-woff&name=public/fonts/[name].[ext]' },
      { test: /\.woff2$/, use: 'url-loader?limit=65000&mimetype=application/font-woff2&name=public/fonts/[name].[ext]' },
      { test: /\.[ot]tf$/, use: 'url-loader?limit=65000&mimetype=application/octet-stream&name=public/fonts/[name].[ext]' },
      { test: /\.eot$/, use: 'url-loader?limit=65000&mimetype=application/vnd.ms-fontobject&name=public/fonts/[name].[ext]' },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[hash].[ext]',
            }
          }
        ]
      }
    ]
  }
};


//Optimize for Production
config.plugins = config.plugins || [];

//Use Production React
config.plugins.push(new webpack.DefinePlugin({
  process: {
    env: {
      NODE_ENV: JSON.stringify("production")
    }
  }
}));
//Minify The Bundle
config.plugins.push(new webpack.optimize.UglifyJsPlugin({
  compress: { warnings: false }
}));


module.exports = config;