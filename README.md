# react-15.x-assessment-basic

## Getting Started
1) Install packages by running `npm install`.
2) Start (E0) local dev environment by running `npm run start:dev`.
3) Open browser to the following url `http://localhost:17000/`.
  > Once `http://localhost:17000/` loads, you should see a list of credit cards

---------------

## Component Review
![Component Overview](./components.png "Component Overview")

###### CardTile A (`composite components`)
```
- Card Image
- Card Title
```

###### CardTile B (`composite components`)
```
- Card Image
- Card Title
- Apply Button
- Card Description
- Learn More Button
- Terms Button
```

###### CardTile C (`composite components`)
```
- Card Image
- Card Description
```

###### CardTile D (`composite components`)
```
- Card Image
- Card Title
- Learn More Button
- Card Description
- Feature Benefit Text
- View Button
```

###### CardTile E (`composite components`)
```
- Card Image
- Card Description
- Benefit List
- Disclaimer
```

###### CardTile F (`composite components`)
```
- Card Image
- Card Title
- Apply Button
- Card Description
- Learn More Button
```

###### CardTile G (`composite components`)
```
- Card Image
- Card Title
- Card Description
- Apply Button
- Learn More Button
```

###### CardTile H (`composite components`)
```
- Card Image
- Card Title
- Card Description
- Apply Button
```

---------------

## User Story
The Project randomly generates 2000+ card tiles. The objective of this assignment is:
- There are eight variants of the card title component. If possible, the candidate should consolidate duplication.
- The source code is not optimized. If possible, the candidate should optimized components, webpack build and service calls.
- The Test Coverage Threshold is set to 65%. If possible, the candidate should increase the test coverage from 0% to 65%.

## Task (`Required`)
- Reduce duplication
- Write one test case

## Task (`Optional`)
- Optimize recursive function that generates components (src/index.web.js) line 46.
- Write JSDocs documentation
- Increase Test Coverage to 85%
- Convert project to a progressive app

## Do's
- Candidate can add any need webpack config and loader.
- Candidate can make any changes to the source code required to complete the task.
- Candidate can use external npm packages

## Dont's
- Candidate cannot use prebuilt templates. .i.g. `create-react-app` `yeoman.io`.


######## last tested on 10/21/2017
