const webpackConfig = require('./webpack.config');
const path = require('path');

const services = require('./__mock__/webpack-dev-services');

module.exports = Object.assign({}, webpackConfig, {
  entry: './__mock__/index.js',
  devServer: {
    port: 17000,
    contentBase: [
      path.join(__dirname, 'dist'),
      path.join(__dirname, 'public'),
      path.join(__dirname, '__mock__'),
    ],
    historyApiFallback: {
      index: 'webpack-dev-server.html'
    },
    before(app){
      app.get('/~/services/creditcards', function(req, res) { res.json(services.creditcards()); });
    }
  }
});
