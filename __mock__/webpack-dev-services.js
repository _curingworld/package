
const loremIpsum = require('lorem-ipsum');

const EOL = '/n';

const cardSubTitle = () => {
  return loremIpsum({
      count: 1                      // Number of words, sentences, or paragraphs to generate.
    , units: 'sentences'            // Generate words, sentences, or paragraphs.
    , sentenceLowerBound: 5         // Minimum words per sentence.
    , sentenceUpperBound: 15        // Maximum words per sentence.
    , paragraphLowerBound: 3        // Minimum sentences per paragraph.
    , paragraphUpperBound: 7        // Maximum sentences per paragraph.
    , format: 'plain'               // Plain text or html
    , words: ['ad', 'dolor']  // Custom word dictionary. Uses dictionary.words (in lib/dictionary.js) by default.
    , random: Math.random           // A PRNG function. Uses Math.random by default
    , suffix: EOL                   // The character to insert between paragraphs. Defaults to default EOL for your OS.
  });
}

const cardDescription = () => {
  return loremIpsum({
      count: 2                      // Number of words, sentences, or paragraphs to generate.
    , units: 'sentences'            // Generate words, sentences, or paragraphs.
    , sentenceLowerBound: 5         // Minimum words per sentence.
    , sentenceUpperBound: 15        // Maximum words per sentence.
    , paragraphLowerBound: 3        // Minimum sentences per paragraph.
    , paragraphUpperBound: 7        // Maximum sentences per paragraph.
    , format: 'plain'               // Plain text or html
    , words: ['ad', 'dolor']  // Custom word dictionary. Uses dictionary.words (in lib/dictionary.js) by default.
    , random: Math.random           // A PRNG function. Uses Math.random by default
    , suffix: EOL                   // The character to insert between paragraphs. Defaults to default EOL for your OS.
  });
}

const footerDisclaimer = () => {
  return loremIpsum({
      count: 2                      // Number of words, sentences, or paragraphs to generate.
    , units: 'sentences'            // Generate words, sentences, or paragraphs.
    , sentenceLowerBound: 5         // Minimum words per sentence.
    , sentenceUpperBound: 15        // Maximum words per sentence.
    , paragraphLowerBound: 3        // Minimum sentences per paragraph.
    , paragraphUpperBound: 7        // Maximum sentences per paragraph.
    , format: 'plain'               // Plain text or html
    , words: ['ad', 'dolor']  // Custom word dictionary. Uses dictionary.words (in lib/dictionary.js) by default.
    , random: Math.random           // A PRNG function. Uses Math.random by default
    , suffix: EOL                   // The character to insert between paragraphs. Defaults to default EOL for your OS.
  });
}

const componentTypes = [
  'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'
];
const componentType = () => {
  return componentTypes[Math.floor(Math.random() * componentTypes.length)];
}

function creditcards() {
  const data = { creditcards: [] }

  for (let i = 0; i < componentTypes.length; i++) {
    data.creditcards.push({
      id: i,
      type: componentTypes[i],
      cardTitle: `creditcard ${i} ${componentTypes[i]}`,
      cardDescription: cardDescription(),
      footerDisclaimer: footerDisclaimer(),
      creditCard1: '/images/credit-card-1.png',
    });
  }



  for (let i = 0; i < 2000; i++) {
    const type = componentType();
    data.creditcards.push({
      id: i,
      type: type,
      cardTitle: `creditcard ${i} ${type}`,
      cardDescription: cardDescription(),
      footerDisclaimer: footerDisclaimer(),
      creditCard1: '/images/credit-card-1.png',
    })
  }

  return data;
}

module.exports = {
  creditcards: creditcards
}