
import './assessment-bootstrap.css';

import React from 'react';
import ReactDOM from 'react-dom';

import WebApp from '../src/index.web';

ReactDOM.render(<WebApp />, document.getElementById('app'));
